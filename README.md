# Protoboard 1

Prototype board with Teensy 3.1, GPIB, CAN and RS232


***

## Description
I wanted a prototype board for playing with some interfaces I had little experience with.
- GPIB, for communicating with some instruments. Proper drivers was a requirement.
- CANbus, for checking whats on the bus in my car and in my heat-pump.
- Serial, just because I like it.

So I created this little board for my experiments.



## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
There is no support for this project. Use if if you like. I have some spare boards. Contact me if you which to have one. While the design is free and open I would need to charge a little for the postage and handling.

## Roadmap
I honestly don't think any more work will be put into this specific board. I have had it produced (by JLCPCB) and at least the GPIB stuff seems to work. After doing some GPIB work I whish I had added some more status LEDs. ATN and NDAV at least...

## Contributing
Ideas for improvements are welcome, but I have no current plans for further development.

If you want to create your own boards based on this design, please feel free to do so. Its a kicad V5 project. If stuff I added to my private library is needed, just ask.

## License
I have not yet decided on this. All my software stuff will be MIT license but this is a hardware project...


## Project status
I would call this project finished. I publish it so that you can use it as a starter for your project if you want.
